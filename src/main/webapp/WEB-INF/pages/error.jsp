<%-- 
    Document   : error
    Created on : Jan 28, 2012, 7:41:16 AM
    Author     : jasoet
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include  page="/WEB-INF/pages/shared/head.jsp"  >
    <jsp:param name="title" value="Error Page"/>
</jsp:include>


<h1>Something Wrong. Contact your Administrator</h1>
<p>
    ${data}
</p>

<jsp:include page="/WEB-INF/pages/shared/foot.jsp"  />
